package com.wyx.jzwd.utils;

import cn.hutool.core.lang.Console;
import cn.hutool.json.JSONUtil;
import com.wyx.jzwd.entity.Result;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@Configuration
public class HttpTools {

    @Value("${website.login}")
    String loginUrl;
    @Value("${jzwd.username}")
    String username;
    @Value("${jzwd.password}")
    String password;
    @Value("${jzwd.orgId}")
    String orgId;
    @Value("${jzwd.ptCode}")
    String ptCode;


    public String getToken() {

        String resultStr = null;
        Map<String,String> map = new HashMap<>();
        map.put("username", username);
        map.put("password", password);
        map.put("orgId", orgId);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = null;
        if (mediaType != null) {
            body = RequestBody.create(mediaType, JSONUtil.toJsonStr(map));
        }
        Request request = new Request.Builder()
                .url(loginUrl)
                .method("POST", body)
                .addHeader("User-Agent", "Apifox/1.0.0 (https://apifox.com)")
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "*/*")
                .addHeader("Host", "wdlm.test.jk.com")
                .addHeader("Connection", "keep-alive")
                .build();
        try {
            Result result = JSONUtil.toBean(client.newCall(request).execute().body().string(), Result.class);
            if (result.isSuccess()) {
                return result.getMsg();
            }

        } catch (IOException e) {
            Console.error("获取toKen异常：" + e);
            throw new RuntimeException(e);
        }

        return null;
    }

    public String post(String url, String token, String jsonStr) {
        String resultStr = null;
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = null;
        if (mediaType != null) {
            body = RequestBody.create(mediaType, jsonStr);
        }
        Request request = new Request.Builder()
                .url(url)
                .method("POST", body)
                .addHeader("User-Agent", "Apifox/1.0.0 (https://apifox.com)")
                .addHeader("Content-Type", "application/json")
                .addHeader("Accept", "*/*")
                .addHeader("Host", "wdlm.test.jk.com")
                .addHeader("Connection", "keep-alive")
                .addHeader("token", token)
                .addHeader("orgid", orgId)
                .addHeader("ptCode", ptCode)
                .build();
        try {

            resultStr = Objects.requireNonNull(client.newCall(request).execute().body()).string();

        } catch (IOException e) {
            Console.error("请求url：" + url + "失败" + e);
            throw new RuntimeException(e);
        }
        Console.log("======================");
        Console.log("请求数据：" + jsonStr);
        Console.log("返回数据：" + resultStr);
        Console.log("======================");
        return resultStr;
    }








}
