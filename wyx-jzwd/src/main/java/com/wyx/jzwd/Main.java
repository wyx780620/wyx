package com.wyx.jzwd;

import com.wyx.jzwd.Service.IGoodsService;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@MapperScan("com.wyx.jzwd.mapper")
@EnableScheduling
public class Main {

    @Autowired
    IGoodsService goodsService;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
        System.out.println("启动成功");


    }
}