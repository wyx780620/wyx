package com.wyx.jzwd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyx.jzwd.entity.OrderDoc;

public interface OrderDocMapper extends BaseMapper<OrderDoc> {
}
