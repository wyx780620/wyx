package com.wyx.jzwd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyx.jzwd.entity.SendDtl;

public interface SendDtlMapper extends BaseMapper<SendDtl> {
}
