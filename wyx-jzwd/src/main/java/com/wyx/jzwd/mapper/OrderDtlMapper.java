package com.wyx.jzwd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyx.jzwd.entity.OrderDtl;

public interface OrderDtlMapper extends BaseMapper<OrderDtl> {
}
