package com.wyx.jzwd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyx.jzwd.entity.Inventory;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.Date;

public interface InventoryMapper extends BaseMapper<Inventory> {

    @Update("update wd_Inventory set is_loaded =#{loaded},loaded_at = #{loadedAt} where com_product_code = #{goodsid}")
    Integer updateStateById(@Param("goodsid") String goodsId, @Param("loaded") Integer loaded, @Param("loadedAt") Date loadedAt);
}
