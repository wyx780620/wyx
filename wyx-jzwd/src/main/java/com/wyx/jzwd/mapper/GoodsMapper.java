package com.wyx.jzwd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyx.jzwd.entity.Goods;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.Date;

public interface GoodsMapper extends BaseMapper<Goods> {

    @Update("update wd_goods set is_loaded =#{loaded},loaded_at = #{loadedAt} where com_product_code = #{goodsid}")
    Integer updateStateById(@Param("goodsid") String goodsId, @Param("loaded") Integer loaded, @Param("loadedAt") Date loadedAt);
}
