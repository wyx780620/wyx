package com.wyx.jzwd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wyx.jzwd.entity.SendDoc;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.util.Date;

public interface SendDoocMapper extends BaseMapper<SendDoc> {
    @Update("update wd_send_doc set is_loaded =#{loaded},loaded_at = #{loadedAt} where document_code = #{goodsid}")
    Integer updateStateById(@Param("goodsid") String goodsId, @Param("loaded") Integer loaded, @Param("loadedAt") Date loadedAt);
}
