package com.wyx.jzwd.controller;

import com.wyx.jzwd.Service.IGoodsService;
import com.wyx.jzwd.Service.IInventoryService;
import com.wyx.jzwd.Service.IOrderService;
import com.wyx.jzwd.Service.ISendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/syn")
public class goodsController {
    @Autowired
    IGoodsService goodsService;
    @Autowired
    IInventoryService inventoryService;
    @Autowired
    IOrderService orderService;

    @Autowired
    ISendService sendService;

    @GetMapping("/goods")
    public String get() {
        goodsService.sny();
        return "同步商品资料请查看服务端日志";
    }


    @GetMapping("/inventory")
    public String inventory() {
        inventoryService.sny();
        return "同步库存请查看日志";
    }

    @GetMapping("/order")
    public String order() {
        orderService.sny();
        return "同步请货单请查看日志";
    }

    @GetMapping("/send")
    public String send() {
        sendService.sny();
        return "同步销售单请查看日志";
    }


}
