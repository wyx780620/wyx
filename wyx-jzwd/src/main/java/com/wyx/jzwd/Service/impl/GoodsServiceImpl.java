package com.wyx.jzwd.Service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wyx.jzwd.Service.IGoodsService;
import com.wyx.jzwd.entity.Goods;
import com.wyx.jzwd.entity.Result;
import com.wyx.jzwd.mapper.GoodsMapper;
import com.wyx.jzwd.utils.HttpTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class GoodsServiceImpl implements IGoodsService {

    @Autowired
    GoodsMapper goodsMapper;

    @Autowired
    HttpTools httpTools;

    @Value("${website.goods}")
    String url;

    @Value("${scheduledConfig.goods}")
    String goods;

    @Override
    public void snyAll() {
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        List<Goods> list = goodsMapper.selectList(queryWrapper);
        sny(list);
    }


    @Override
    @Scheduled(cron = "${scheduledConfig.goods}")
    public void sny() {
        QueryWrapper<Goods> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_loaded", 0);
        List<Goods> list = goodsMapper.selectList(queryWrapper);
        sny(list);

    }


    private void sny(List<Goods> list) {
        if (BeanUtil.isNotEmpty(list)) {
            String toKen = httpTools.getToken();
            if (BeanUtil.isNotEmpty(toKen)) {
                for (Goods goods : list) {
                    String resultStr = httpTools.post(url, toKen, JSONUtil.toJsonStr(goods));
                    Result result = JSONUtil.toBean(resultStr, Result.class);
                    if (result.isSuccess()) {
                        goods.setIsLoaded(1);
                        goods.setLoadedAt(new Date());
                        goodsMapper.updateStateById(goods.getComProductCode(), goods.getIsLoaded(), goods.getLoadedAt());
                    } else {
                        Console.error(result);
                    }
                }
            }

        }
    }
}
