package com.wyx.jzwd.Service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wyx.jzwd.Service.IInventoryService;
import com.wyx.jzwd.entity.Inventory;
import com.wyx.jzwd.entity.Result;
import com.wyx.jzwd.mapper.InventoryMapper;
import com.wyx.jzwd.utils.HttpTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class InventoryServiceImpl implements IInventoryService {


    @Autowired
    InventoryMapper inventoryMapper;

    @Autowired
    HttpTools httpTools;

    @Value("${website.inventory}")
    String url;


    @Override
    public void snyAll() {
        QueryWrapper<Inventory> queryWrapper = new QueryWrapper<>();
        List<Inventory> list = inventoryMapper.selectList(queryWrapper);
        sny(list);

    }

    @Override
    @Scheduled(cron = "${scheduledConfig.inventory}")
    public void sny() {
        QueryWrapper<Inventory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("is_loaded", 0);
        List<Inventory> list = inventoryMapper.selectList(queryWrapper);
        sny(list);

    }


    private void sny(List<Inventory> list) {
        if (BeanUtil.isNotEmpty(list)) {
            String toKen = httpTools.getToken();
            if (BeanUtil.isNotEmpty(toKen)) {
                for (Inventory inventory : list) {
                    String resultStr = httpTools.post(url, toKen, JSONUtil.toJsonStr(inventory));
                    Result result = JSONUtil.toBean(resultStr, Result.class);
                    if (result.isSuccess()) {
                        inventory.setIsLoaded(1);
                        inventory.setLoadedAt(new Date());
                        inventoryMapper.updateStateById(inventory.getComProductCode(), inventory.getIsLoaded(), inventory.getLoadedAt());
                    } else {
                        Console.error(result);
                    }
                }
            }

        }
    }
}
