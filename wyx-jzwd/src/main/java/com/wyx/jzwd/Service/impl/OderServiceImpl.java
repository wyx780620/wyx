package com.wyx.jzwd.Service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wyx.jzwd.Service.IOrderService;
import com.wyx.jzwd.entity.OrderDoc;
import com.wyx.jzwd.entity.OrderDtl;
import com.wyx.jzwd.entity.Result;
import com.wyx.jzwd.mapper.OrderDocMapper;
import com.wyx.jzwd.mapper.OrderDtlMapper;
import com.wyx.jzwd.utils.HttpTools;
import com.wyx.jzwd.vo.CallVo;
import com.wyx.jzwd.vo.OrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class OderServiceImpl implements IOrderService {

    @Autowired
    OrderDocMapper docMapper;

    @Autowired
    OrderDtlMapper dtlMapper;

    @Autowired
    HttpTools httpTools;

    @Value("${website.order}")
    String url;

    @Value("${website.callbak}")
    String bakUrl;


    @Value("${jzwd.yewType}")
    List<String> yewTypes;

    @Override
    public void snyAll() {

    }

    @Override
    @Scheduled(cron = "${scheduledConfig.order}")
    public void sny() {
        List<CallVo> callVos = new ArrayList<>();
        String toKen = httpTools.getToken();
        Map param = new HashMap<>();
        Date date = new Date();

        for (String yewType : yewTypes) {
            param.put("startTime", DateUtil.formatDateTime(DateUtil.offsetDay(date, -7)));
            param.put("endTime", DateUtil.formatDateTime(date));
            param.put("yewType", yewType);
            Result<List> result = JSONUtil.toBean(httpTools.post(url, toKen, JSONUtil.toJsonStr(param)), Result.class);
            List<OrderVo> vos = result.getData();

            if (!vos.isEmpty()) {
                for (Object obj : vos) {
                    OrderVo vo = BeanUtil.toBean(obj, OrderVo.class);
                    saveOrder(vo);
                    CallVo callVo = new CallVo();
                    callVo.setBillNo(vo.getBillNo());
                    callVo.setYewType(vo.getYewType());
                    callVo.setStatus(1);
                    callVos.add(callVo);
                }
                callback(toKen, callVos);
            }
        }
    }

    @Transactional
    public void saveOrder(OrderVo vo) {
        if (BeanUtil.isNotEmpty(vo)) {
            OrderDoc doc = BeanUtil.toBean(vo, OrderDoc.class);

            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("bill_no", doc.getBillNo());
            queryWrapper.eq("yew_type", doc.getYewType());
            if (docMapper.selectCount(queryWrapper) == 0) {
                doc.setIsLoaded(0);
                doc.setLoadedAt(DateUtil.date());
                docMapper.insert(doc);
                for (OrderDtl dtl : vo.getDtList()) {
                    dtl.setBillNo(doc.getBillNo());
                    if (BeanUtil.isNotEmpty(dtl)) {
                        dtlMapper.insert(dtl);
                    }
                }
            }
        }
    }

    /**
     * @param token
     */
    private void callback(String token, List<CallVo> callVos) {
        String result = httpTools.post(bakUrl, token, JSONUtil.toJsonStr(callVos));
    }


}
