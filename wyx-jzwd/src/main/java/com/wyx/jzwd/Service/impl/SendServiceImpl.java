package com.wyx.jzwd.Service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wyx.jzwd.Service.ISendService;
import com.wyx.jzwd.entity.Result;
import com.wyx.jzwd.entity.SendDoc;
import com.wyx.jzwd.entity.SendDtl;
import com.wyx.jzwd.mapper.SendDoocMapper;
import com.wyx.jzwd.mapper.SendDtlMapper;
import com.wyx.jzwd.utils.HttpTools;
import com.wyx.jzwd.vo.SendVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SendServiceImpl implements ISendService {

    @Autowired
    SendDoocMapper docMapper;

    @Autowired
    SendDtlMapper dtlMapper;

    @Autowired
    HttpTools httpTools;

    @Value("${website.pushOrder}")
    String url;

    @Override
    public void snyAll() {

    }

    @Override
    @Scheduled(cron = "${scheduledConfig.send}")
    public void sny() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("is_loaded", 0);
        List<SendDoc> listDoc = docMapper.selectList(queryWrapper);
        List<SendVo> vos = new ArrayList<>();
        if (BeanUtil.isNotEmpty(listDoc)) {
            for (SendDoc doc : listDoc) {
                SendVo vo = BeanUtil.toBean(doc, SendVo.class);
                QueryWrapper dtlWrapper = new QueryWrapper();
                dtlWrapper.eq("document_code", doc.getDocumentCode());
                List<SendDtl> dtls = dtlMapper.selectList(dtlWrapper);
                if (BeanUtil.isNotEmpty(dtls)) {
                    vo.setDtList(dtls);
                }
                vos.add(vo);
            }

            sny(vos);

        }

    }

    private void sny(List<SendVo> list) {
        if (BeanUtil.isNotEmpty(list)) {
            String toKen = httpTools.getToken();
            if (BeanUtil.isNotEmpty(toKen)) {
                for (SendVo vo : list) {
                    String resultStr = httpTools.post(url, toKen, JSONUtil.toJsonStr(vo));
                    Result result = JSONUtil.toBean(resultStr, Result.class);
                    if (result.isSuccess()) {
                        docMapper.updateStateById(vo.getDocumentCode(), 1, new Date());
                    } else {
                        Console.error(result);
                    }
                }
            }

        }
    }
}
