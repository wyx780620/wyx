package com.wyx.jzwd.Service;

public interface ISendService {
    /**
     * 同步所有数据
     */
    void snyAll();

    /**
     * 同步未同步数据
     */
    void sny();
}
