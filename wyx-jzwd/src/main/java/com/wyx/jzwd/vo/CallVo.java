package com.wyx.jzwd.vo;

import lombok.Data;

@Data
public class CallVo {
    Integer billNo;
    String yewType;
    Integer status;
}
