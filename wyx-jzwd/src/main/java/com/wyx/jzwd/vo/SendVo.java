package com.wyx.jzwd.vo;

import com.wyx.jzwd.entity.SendDtl;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class SendVo {
    String documentCode;
    String relaDocumentCode;
    String yewType;
    String customerNo;
    String customerName;
    String kpy;
    String dates;
    String times;
    BigDecimal taxAmount;
    BigDecimal amount;
    String remark;
    List<SendDtl> dtList;
}
