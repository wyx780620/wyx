package com.wyx.jzwd.vo;

import com.wyx.jzwd.entity.OrderDtl;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderVo {
    String entId;
    String orgId;
    String customerNo;
    String yewType;
    Integer billNo;
    String billCode;
    String dates;
    String times;
    BigDecimal taxAmount;
    String remark;
    List<OrderDtl> dtList;
}
