package com.wyx.jzwd.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GoodsVo {
    String comProductCode;
    String productCode;
    String goodsName;
    String zjm;
    String generalName;
    String specification;
    String unit;
    String provider;
    String habitat;
    BigDecimal taxRate;
    String storageSort;
    String fSort;
    String grantNumber;
    String grantDate;
    String packageCount;
    Integer isSplit;
    String purchaser;
    BigDecimal fzdcbjpf;
    String barCode;
    Integer logOff;
    String recipeSort;
    String memo;
}
