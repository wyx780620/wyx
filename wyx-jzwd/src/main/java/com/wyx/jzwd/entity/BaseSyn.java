package com.wyx.jzwd.entity;

import lombok.Data;

import java.util.Date;

@Data
public class BaseSyn {
    Integer isLoaded;
    Date loadedAt;
}
