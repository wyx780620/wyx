package com.wyx.jzwd.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("wd_Order_dtl")
public class OrderDtl {
    Integer billSn;
    String goodsId;
    String goodsCode;
    String fcomProductCode;
    String fproductCode;
    String goodsName;
    String goodsSpec;
    String unit;
    String manufacturer;
    String batchCode;
    String produceDate;
    String valDate;
    BigDecimal nums;
    BigDecimal taxPrice;
    BigDecimal taxAmount;
    String returnReason;
    String reDocumentCode;
    Integer billNo;
}
