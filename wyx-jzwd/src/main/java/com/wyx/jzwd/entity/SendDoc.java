package com.wyx.jzwd.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("wd_send_doc")
public class SendDoc extends BaseSyn {
    String documentCode;
    String relaDocumentCode;
    String yewType;
    String customerNo;
    String customerName;
    String kpy;
    String dates;
    String times;
    BigDecimal taxAmount;
    BigDecimal amount;
    String remark;
}
