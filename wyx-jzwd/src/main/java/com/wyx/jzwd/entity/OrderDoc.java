package com.wyx.jzwd.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("wd_Order_doc")
public class OrderDoc extends BaseSyn {

    String entId;
    String orgId;
    String customerNo;
    String yewType;
    Integer billNo;
    String billCode;
    String dates;
    String times;
    BigDecimal taxAmount;
    String remark;


}
