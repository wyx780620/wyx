package com.wyx.jzwd.entity;

import lombok.Data;

@Data
public class Result<T> {
    boolean success;
    Integer code;
    String msg;
    T data;
}
