package com.wyx.jzwd.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("wd_inventory")
public class Inventory extends BaseSyn {
    String comProductCode;
    String productCode;
    String batchcode;
    BigDecimal flshj;
    String productiondate;
    String period;
    BigDecimal count;


}
