package com.wyx.jzwd.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;

@Data
@TableName("wd_send_dtl")
public class SendDtl {
    String detailsNumber;
    String comProductCode;
    String productCode;
    String goodsCode;
    String batchCode;
    String produceDate;
    String valDate;
    BigDecimal taxAmount;
    BigDecimal amount;
    BigDecimal nums;
    BigDecimal taxPrice;
    BigDecimal price;

}
