package com.wyx.common.config;

/**
 * @Program: study
 * @ClassName: MybatisPlusConfig
 * @Description:
 * @Auther: wyx
 * @Date: 2022/2/22 19:05
 * @Version: 1.0
 */

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 分页插件配置类
 */
@ConditionalOnClass(value = {PaginationInnerInterceptor.class})
@Configuration
public class MybatisPlusConfig {

    /**
     * 分页插件
     *
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.ORACLE));
        return interceptor;
    }

    /**
     * @Author wyx
     * @Description 添加批量插入
     * @Date 11:21 2022/2/15
     * @Param
     * @Return: com.javaweb.common.common.EasySqlInjector
     */
  /*  @Bean
    public EasySqlInjector easySqlInjector() {
        return new EasySqlInjector();
    }*/


}
